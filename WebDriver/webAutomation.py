# import packages - main ones are selenium and webdriver
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
import time

# optional code as this keeps the web browser open even after the script finishes running otherwise it will automatically close
options = Options()
options.add_experimental_option("detach", True)

# driver connect you to chrome browser.
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()),
                          options=options)

# get the website
driver.get("https://www.neuralnine.com/")

# maximise browser window automatically when code is run
driver.maximize_window()

# find the href for book nav element
bookNavButton = driver.find_element("xpath",
                                    "//li[contains(@class, "
                                    "'menu-item menu-item-type-post_type menu-item-object-page menu-item-715')] "
                                    "//a")
# click the href
bookNavButton.click()

# find the href for the book containing text '7 IN 1'
bookButton = driver.find_element(
    "xpath", "//div[contains(@class, 'elementor-widget-wrap')][.//h2[text()[contains(., '7 IN 1')]]][count(.//a)=2]//a")

# click the href
bookButton.click()

# when the new window opens transfer selenium operations to the new window
driver.switch_to.window(driver.window_handles[1])
time.sleep(3)

# find element that conatins the price of the book
priceButton = driver.find_element("xpath", "//a[.//span[text()[contains(., 'Paperback')]]] //span[text()[contains(., '$')]]")

# get innerHTML of the element containing price of the book.
for button in priceButton.get_attribute("innerHTML"):
    print(button)
