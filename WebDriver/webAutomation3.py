import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By

# keep browser window open even after script finishes running
options = Options()
options.add_experimental_option("detach", True)

# set up the driver that will connect you to chrome and its services
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()),
                          options=options)
# get the website and open a browser window in chrome
driver.get("https://docs.google.com/forms/d/e/1FAIpQLSeguXuXp4yjVyjitpOxEotD8Q0mSm4wzgwgaSmrDoqQPzXA9g/viewform")
# find sign in button element and click to continue sign in
signInButton = driver.find_element("xpath", "/html/body/div[2]/div/div[2]/div[3]/div[2]")
signInButton.click()

# allow the page to finish loading before the script starts finding elements
time.sleep(3)

# put email address in input element
email = "fakhruddin.dahodwala@students.mq.edu.au"
# find the input element
emailPlace = driver.find_element("xpath", "//*[@id='identifierId']")
# send email address string to input field
emailPlace.send_keys(email)

# find the next button in email page and click
nextEmail = driver.find_element("xpath", "//*[@id='identifierNext']/div/button")
nextEmail.click()

# wait for browser to finish loading the next page
time.sleep(3)

# put password in input element
password = "Babu1350!"
# find the input element
passwordPlace = driver.find_element("xpath", "//*[@id='password']/div[1]/div/div[1]/input")
# send password string to input field
passwordPlace.send_keys(password)

time.sleep(3)

# find the next button in password page and click
nextPassword = driver.find_element("xpath", "//*[@id='passwordNext']/div/button")
nextPassword.click()

time.sleep(3)

# find email input element and add email string in it
emailPlace = driver.find_element("xpath", "//*[@id='mG61Hd']/div[2]/div/div[2]/div[1]/div/div[1]/div[2]/div[1]/div/div[1]/input")
emailPlace.send_keys(email)

# find full name input element and add full name string in it
fullName = "Fahrul Moiz"
fullNamePlace = driver.find_element("xpath", "//*[@id='mG61Hd']/div[2]/div/div[2]/div[2]/div/div/div[2]/div/div[1]/div/div[1]/input")
fullNamePlace.send_keys(fullName)

# find room number input element and add room number string in it
roomNumber = "A121"
roomNumberPlace = driver.find_element("xpath", '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[3]/div/div/div[2]/div/div[1]/div/div[1]/input')
roomNumberPlace.send_keys(roomNumber)

time.sleep(3)
checkBox1 = driver.find_elements(By.CLASS_NAME, "eBFwI")


for check in checkBox1:
    if "SATURDAY DAY" in check.get_attribute("innerHTML"):
        print("nope")
    elif "SATURDAY NIGHT" in check.get_attribute("innerHTML"):
        print("nope")
    elif "SUNDAY DAY" in check.get_attribute("innerHTML"):
        print("nope")
    elif "SUNDAY NIGHT" in check.get_attribute("innerHTML"):
        print("nope")
    elif "I am NOT available to work this week" in check.get_attribute("innerHTML"):
        print("nope")
    else:
        check.click()

shift1 = "1"
shift1Place = driver.find_element("xpath", "//*[@id='mG61Hd']/div[2]/div/div[2]/div[5]/div/div/div[2]/div/div[1]/div/div[1]/input")
shift1Place.send_keys(shift1)

shift2 = "1"
shift2Place = driver.find_element("xpath", "//*[@id='mG61Hd']/div[2]/div/div[2]/div[7]/div/div/div[2]/div/div[1]/div/div[1]/input")
shift2Place.send_keys(shift2)

finalComments = "None"
finalCommentsPlace = driver.find_element("xpath", "//*[@id='mG61Hd']/div[2]/div/div[2]/div[8]/div/div/div[2]/div/div[1]/div[2]/textarea")
finalCommentsPlace.send_keys(finalComments)

submitButton = driver.find_element("xpath", "//*[@id='mG61Hd']/div[2]/div/div[3]/div[3]/div[1]/div")
submitButton.click()